function sum(arg1, arg2 /*, ...*/) {
	var i = 0,
		l = arguments.length,
		total = 0;
	for (; i < l; ++i) {
		total += Number(arguments[i]);
	}
	return total;
}

function foo() {
	var i = 1;
}
