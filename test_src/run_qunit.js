// webpageモジュールを読み込み
var page = require('webpage').create(),
// fileSystemモジュールを読み込み
	fs = require('fs'),
// jQueryのソース
	jQuerySrc = "http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js",
// テストしたいページ(ルートからの相対パス)
	targetUrl = "./test_src/qunit/index.html";

page.open("file://" + fs.absolute(targetUrl), function (status) {
	if (status !== 'success') {
		console.log('failed to load the targetUrl');
		phantom.exit();
	} else {
		// includeJsは外部のJavaScript(第一引数)を取得し、
		// 読み込み完了したらコールバック(第二引数)を呼ぶ。
		page.includeJs(jQuerySrc, function() {
			// evaluateは対象のページのコンテキストで実行する。
			var fails = page.evaluate(function() {
				// 失敗テストの個数を数える
				return $("ol#qunit-tests > li.fail").length;
			});
			// phantomプロセスの終了(終了コードはエラーの個数)。
			phantom.exit(fails);
		});
	}
});

// 対象ページのコンテキストでのコンソール出力をハンドリング
page.onConsoleMessage = function(msg, line, src) {
	console.log(msg);
};

