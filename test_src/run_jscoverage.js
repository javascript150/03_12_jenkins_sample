// webpageモジュールを読み込み
var page = require('webpage').create(),
// fileSystemモジュールを読み込み
	fs = require('fs'),
// jQueryのソース
	jQuerySrc = "http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js";

if (phantom.args.length !== 1) {
	console.log("Usage: run_jscoverage.js <JSCOVERAGE_URL>");
	phantom.exit();
}

page.open(phantom.args[0], function (status) {
	if (status !== 'success') {
		console.log('failed to load "' + phantom.args[0] + '"');
		phantom.exit();
	} else {
		// includeJsは外部のJavaScript(第一引数)を取得し、
		// 読み込み完了したらコールバック(第二引数)を呼ぶ。
		page.includeJs(jQuerySrc, function() {
			// evaluateは対象のページのコンテキストで実行する。
			page.evaluate(function() {
				$.ajax({
					type: "POST",
					url: "/jscoverage-store",
					data: jscoverage_serializeCoverageToJSON(),
					async: false
				}).done(function(a) {
					console.log("jscoverage store succeeded.");
				}).fail(function(a) {
					console.log("jscoverage store failed.");
				});
			});
			phantom.exit();
		});
	}
});

