test("sum", function() {
	equal(sum(1, 2), 3,  "1 + 2");
	equal(sum(1, 2, 3), 6,  "1 + 2 + 3");
	equal(sum(1, 2, 3, 4), 10,  "1 + 2 + 3 + 4");
	equal(sum(1, "2"), 3,  "文字列でも大丈夫");
});

